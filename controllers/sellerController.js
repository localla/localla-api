const { user } = require('../models');
const bcrypt = require('bcrypt');
const Validator = require('fastest-validator');
const v = new Validator();
const { imagekit } = require('../helpers');

module.exports = {
    registerSeller: async (req, res) => {
        try {
            const User_id = req.user.id;;
            const User = await user.findOne({ where: { id: User_id } })
            if (!User) {
                return res.status(400).json({
                    status: false,
                    message: 'user does not exist'
                })
            }

            const { nameShop, imageShop, province, city, address } = req.body;
            let query = { where: { id: User_id } }

            if (req.file) {
                const file = req.file.buffer.toString("Base64");
                const namaFile = Date.now() + '-' + req.file.originalname;

                if (req.file.size > 5 * 1024 * 1024) {
                    return res.status(400).json({
                        status: false,
                        message: 'Size too large, max 5MB'
                    })
                }

                if (req.file.mimetype !== 'image/png' && req.file.mimetype !== 'image/jpg' && req.file.mimetype !== 'image/jpeg') {
                    return res.status(400).json({
                        status: false,
                        message: "Supported image files are jpeg, jpg, and png"
                    })
                }

                const upload = await imagekit.upload({
                    file: file,
                    fileName: namaFile,
                    folder: '/localla/seller/images'
                });

                let updated = await user.update({
                    nameShop,
                    province,
                    city,
                    address,
                    imageShop: upload.url,
                    role: 'seller'
                }, query)

                res.status(200).json({
                    status: true,
                    message: 'change profile updated',
                    data: updated
                })
            } else {
                let updated = await user.update({
                    nameShop,
                    province,
                    city,
                    address,
                    role: 'seller'
                }, query)

                res.status(200).json({
                    status: true,
                    message: 'change profile updated',
                    data: updated
                })
            }
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },
}
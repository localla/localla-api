const { product, user, product_images, wishlist, transaction, wishlist_count } = require("../models");
const Validator = require("fastest-validator");
const v = new Validator();
const { imagekit } = require('../helpers');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = {
  createProduct: async (req, res) => {
    try {
      const User_id = req.user.id;

      const User = await user.findOne({ where: { id: User_id } })
      if (!User) {
        return res.status(400).json({
          status: false,
          message: 'User is not exists'
        })
      }

      if ((User.phone && User.city && User.address && User.nameShop && User.imageShop) == null) {
        return res.status(400).json({
          status: false,
          message: 'Profile must be complete'
        })
      }

      const productMaxfive = await product.findAll({ where: { user_id: User_id, status: 'Available' } })
      if (productMaxfive.length >= 4) {
        return res.status(400).json({
          status: false,
          message: 'Maximum selling posts is 4'
        })
      }

      const schema = {
        title: "string|min:1",
        category: "string|optional",
        description: "string|optional",
        price: "string",
      };

      const validate = v.validate(req.body, schema);
      if (validate.length) {
        return res.status(400).json({
          status: false,
          message: "Bad Request",
          data: validate,
        });
      }

      const { title, category, description, price } = req.body;

      let imageArray = req.files;

      if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
          status: false,
          message: "No files were uploaded.",
          data: null
        });
      }

      if (!req.files || Object.keys(req.files).length > 4) {
        return res.status(400).json({
          status: false,
          message: "Maximum upload of 4 files.",
          data: null
        });
      }

      var pImages = [];
      for (let f in imageArray) {
        if (imageArray[f].mimetype !== 'image/png' && imageArray[f].mimetype !== 'image/jpg' && imageArray[f].mimetype !== 'image/jpeg') {
          return res.status(400).json({
            status: false,
            message: "Supported image files are jpeg, jpg, and png"
          });
        }
        if (imageArray[f].size > 5 * 1024 * 1024) {
          return res.status(400).json({
            status: false,
            message: "Size too large, max 5MB"
          });
        }

        const upload = await imagekit.upload({
          file: imageArray[f].buffer,
          fileName: Date.now() + '-' + imageArray[f].originalname,
          folder: '/localla/product/images'
        })
        pImages.push(upload.url)
      }
      const newImagesProduct = await product_images.create({
        url: pImages,
      });

      const newProduct = await product.create({
        user_id: User_id,
        title,
        category,
        description,
        price,
        image: newImagesProduct.id,
        status: 'Available',
      });

      const countWishlist = await wishlist_count.create({
        product_id: newProduct.id,
        total_wishlist: 0
      })

      pImages = [];
      return res.status(201).json({
        status: true,
        message: "New Product Created",
        data: newProduct,
      });
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  detailProduct: async (req, res) => {
    try {
      const product_id = req.params.productid;

      const detail_product = await product.findOne({
        include: [
          {
            model: user,
            as: "owner",
            attributes: ['nameShop', 'province', 'city', 'imageShop']
          },
          {
            model: product_images,
            as: "image_url",
            attributes: ['url']
          },
          {
            model: wishlist_count,
            as: 'productWishlist',
            attributes: ['total_wishlist']
          },
        ],
        where: { id: product_id },
      });

      if (!detail_product) {
        return res.status(404).json({
          status: false,
          message: `Product doesn\'t exist!`,
          data: null,
        });
      }


      res.status(200).json({
        status: true,
        message: "Product Detail",
        data: detail_product
      });
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  allProduct: async (req, res) => {
    try {
      const search = req.query.q;
      // let page = req.query.page;
      // let perPage = req.query.perPage;
      // if (!perPage) {
      //   perPage = 20;
      // }
      // if (!page) {
      //   page = 1;
      // }
      if (search) {
        // let products = await product.findAndCountAll({
        let products = await product.findAll({
          where: {
            [Op.or]: [
              {
                category: {
                  [Op.iLike]: `%${search}%`
                }
              },
              {
                title: {
                  [Op.iLike]: `%${search}%`
                }
              }
            ]
          },
          // limit: perPage,
          // offset: (page - 1) * perPage,
          include: [
            {
              model: user,
              as: "owner",
              attributes: ['nameShop', 'province', 'city', 'imageShop']
            },
            {
              model: product_images,
              as: "image_url",
              attributes: ['url']
            },
            {
              model: wishlist_count,
              as: 'productWishlist',
              attributes: ['total_wishlist']
            },
          ],
          order: [
            ['id', 'DESC'],
          ],
        });

        // let prev_page_url = `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path}?page=${Number(page) - 1}`;
        // if (prev_page_url == `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path}?page=0`) {
        //   prev_page_url = null
        // } else {
        //   prev_page_url = `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path}?page=${Number(page) - 1}`
        // }

        // let response = {
        //   products: products.rows,
        //   total: products.count,
        //   current_page: page,
        //   first_page_url: `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path
        //     }?page=1`,
        //   prev_page_url: prev_page_url,
        //   next_page_url: `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path
        //     }?page=${Number(page) + 1}`,
        // };
        return res.status(200).json({
          status: true,
          message: "All Data Product",
          data: products,
        });
      } else {
        // let products = await product.findAndCountAll({
        let products = await product.findAll({
          // limit: perPage,
          // offset: (page - 1) * perPage,
          include: [
            {
              model: user,
              as: "owner",
              attributes: ['nameShop', 'province', 'city', 'imageShop']
            },
            {
              model: product_images,
              as: "image_url",
              attributes: ['url']
            },
            {
              model: wishlist_count,
              as: 'productWishlist',
              attributes: ['total_wishlist']
            },
          ],
          order: [
            ['id', 'DESC'],
          ],
        });

        // let prev_page_url = `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path}?page=${Number(page) - 1}`;
        // if (prev_page_url == `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path}?page=0`) {
        //   prev_page_url = null
        // } else {
        //   prev_page_url = `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path}?page=${Number(page) - 1}`
        // }

        // let response = {
        //   products: products.rows,
        //   total: products.count,
        //   current_page: page,
        //   first_page_url: `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path
        //     }?page=1`,
        //   prev_page_url: prev_page_url,
        //   next_page_url: `${req.protocol}://${req.get("host")}${req.baseUrl}${req.path
        //     }?page=${Number(page) + 1}`,
        // };
        return res.status(200).json({
          status: true,
          message: "All Data Product",
          data: products,
        });
      }
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  allProductByUserId: async (req, res) => {
    try {
      const user_id = req.params.userid;

      let products = await product.findAll({
        where: {
          user_id: user_id
        },
        include: [
          {
            model: user,
            as: "owner",
            attributes: ['nameShop', 'province', 'city', 'imageShop']
          },
          {
            model: product_images,
            as: "image_url",
            attributes: ['url']
          },
          {
            model: wishlist_count,
            as: 'productWishlist',
            attributes: ['total_wishlist']
          },
        ],
        order: [
          ['id', 'DESC'],
        ],
      });

      return res.status(200).json({
        status: true,
        message: "All Data Product by seller",
        data: products,
      });
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  updateProduct: async (req, res) => {
    try {
      const product_id = req.params.productid;

      const update_product = await product.findOne({
        where: { id: product_id },
      });

      if (!update_product) {
        return res.status(404).json({
          status: false,
          message: `Product doesn\'t exist!`,
          data: null,
        });
      }

      const { title, category, description, price, status, image1, image2, image3, image4 } = req.body;

      let imageArray = req.files;

      if (!imageArray) {
        const updated = await product.update(
          {
            title,
            category,
            description,
            price,
            status,
          },
          {
            where: { id: product_id },
          }
        );

        return res.status(200).json({
          status: true,
          message: "Product Updated",
          data: updated,
        });
      }
      else {
        if (!req.files || Object.keys(req.files).length > 4) {
          return res.status(400).json({
            status: false,
            message: "Maximum upload of 4 files.",
            data: null
          });
        }

        const findImages = await product_images.findOne({
          where: { id: update_product.image },
        });

        var pImages = [];
        if (image1 != null) pImages.push(image1)
        if (image2 != null) pImages.push(image2)
        if (image3 != null) pImages.push(image3)
        if (image4 != null) pImages.push(image4)
        for (let f in imageArray) {
          if (imageArray[f].mimetype !== 'image/png' && imageArray[f].mimetype !== 'image/jpg' && imageArray[f].mimetype !== 'image/jpeg') {
            return res.status(400).json({
              status: false,
              message: "Supported image files are jpeg, jpg, and png"
            });
          }
          if (imageArray[f].size > 5 * 1024 * 1024) {
            return res.status(400).json({
              status: false,
              message: "Size too large, max 5MB"
            });
          }

          const upload = await imagekit.upload({
            file: imageArray[f].buffer,
            fileName: Date.now() + '-' + imageArray[f].originalname,
            folder: '/localla/product/images'
          })
          pImages.push(upload.url)
        }

        const newImagesProduct = await product_images.update({
          url: pImages,
        },
          {
            where:
              { id: update_product.image }
          }
        );

        const updated = await product.update(
          {
            title,
            category,
            description,
            price,
            image: newImagesProduct.id,
            status,
          },
          {
            where: { id: product_id },
          }
        );

        pImages = [];
        return res.status(200).json({
          status: true,
          message: "Product Updated",
          data: updated,
        });
      }
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  deleteProduct: async (req, res) => {
    try {
      const product_id = req.params.productid;

      const delete_product = await product.findOne({
        where: { id: product_id },
      });

      if (!delete_product) {
        return res.status(404).json({
          status: false,
          message: `Product doesn\'t exist!`,
          data: null,
        });
      }

      const deleted = await product.destroy({
        where: { id: product_id },
      });

      res.status(200).json({
        status: true,
        message: "Product Deleted",
        data: deleted,
      });
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  wishlist: async (req, res) => {
    try {
      const user_id = req.user.id;
      const User = await user.findOne({ where: { id: user_id } })
      if (!User) {
        return res.status(400).json({
          status: false,
          message: 'user does not exist'
        })
      }

      const product_id = req.params.productid;
      const productfind = await product.findOne({
        where: { id: product_id },
      });

      if (!productfind) {
        return res.status(404).json({
          status: false,
          message: `Product doesn\'t exist!`,
          data: null,
        });
      }


      const wishlistfind = await wishlist.findOne({
        where: {
          user_id: user_id,
          product_id: product_id
        }
      })

      const totalWishlist = await wishlist_count.findOne({
        where: {
          product_id: product_id
        }
      })

      if (!wishlistfind) {
        const createWishlist = await wishlist.create({
          user_id: user_id,
          product_id: product_id,
          seller_id: productfind.user_id
        })


        const countWishlist = await wishlist_count.update(
          {
            total_wishlist: totalWishlist.total_wishlist + 1
          },
          {
            where: { product_id: product_id },
          }
        )

        return res.status(200).json({
          status: true,
          message: "Create Wishlist successfully",
          data: createWishlist,
        });
      } else {
        const wishlistUpdate = await wishlist.destroy({
          where: { id: wishlistfind.id },
        })

        const countWishlist = await wishlist_count.update(
          {
            total_wishlist: totalWishlist.total_wishlist - 1
          },
          {
            where: { product_id: product_id },
          }
        )

        return res.status(200).json({
          status: true,
          message: "Remove Wishlist successfully",
          data: wishlistUpdate,
        });
      }
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

};

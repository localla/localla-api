const { user } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const helper = require('../helpers/google')
const { google } = require('googleapis');
const open = require('open');

const {
    OAUTH_CLIENT_ID,
    OAUTH_CLIENT_SECRET,
    SERVER_ROOT_URI,
    JWT_SECRET_KEY,
    SERVER_LOGIN_ENDPOINT
} = process.env;

const oauth2Client = new google.auth.OAuth2(
    OAUTH_CLIENT_ID,
    OAUTH_CLIENT_SECRET,
    `${SERVER_ROOT_URI}/${SERVER_LOGIN_ENDPOINT}`
);

function generateAuthUrl() {
    const scopes = [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile'
    ];

    const url = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        response_type: 'code',
        scope: scopes
    });

    return url;
}

async function setCredentials(code) {
    return new Promise(async (resolve, rejects) => {
        try {
            const { tokens } = await oauth2Client.getToken(code);
            oauth2Client.setCredentials(tokens);

            return resolve(tokens);
        } catch (err) {
            return rejects(err);
        }
    });
}

function getUserInfo() {
    return new Promise(async (resolve, rejects) => {
        try {
            var oauth2 = google.oauth2({
                auth: oauth2Client,
                version: 'v2'
            });

            const data = oauth2.userinfo.get((err, res) => {
                if (err) {
                    return rejects(err);
                } else {
                    return resolve(res);
                }
            });
        } catch (err) {
            return rejects(err);
        }
    });
}


module.exports = {
    login: async (req, res) => {
        try {
            const { email, password } = req.body;

            const isUserExists = await user.findOne({ where: { email } });
            if (!isUserExists) {
                return res.status(400).json({
                    status: false,
                    message: 'Email is not found',
                    data: null
                })
            }

            const passwordCorrect = await bcrypt.compare(req.body.password, isUserExists.password);
            if (!passwordCorrect) {
                return res.status(400).json({
                    status: false,
                    message: 'Wrong password!',
                    data: null
                })
            }

            const payload = {
                id: isUserExists.id,
                email: isUserExists.email,
                role: isUserExists.role
            };

            const secretKey = process.env.JWT_SECRET_KEY;
            const tokenAccess = jwt.sign(payload, secretKey);

            res.status(200).json({
                status: true,
                message: 'Login success',
                data: payload,
                tokenAccess
            })
        } catch (err) {
            res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    googleOAuth: async (req, res) => {
        try {
            const code = req.query.code;

            if (!code) {
                const loginUrl = generateAuthUrl();
                return res.redirect(loginUrl);
                // return open(loginUrl)
            }

            await setCredentials(code);

            const { data } = await getUserInfo();
            const findEmail = await user.findOne({ where: { email: data.email } });
            if (!findEmail) {
                const newUser = await user.create({
                    name: data.name,
                    email: data.email,
                    phone: null,
                    city: null,
                    address: null,
                    image: data.picture,
                    role: 'buyer',
                    userType: 'google'
                });

                const payload = {
                    id: newUser.id,
                    email: newUser.email,
                    role: newUser.role
                };

                const tokenAccess = jwt.sign(payload, JWT_SECRET_KEY);

                return res.status(200).json({
                    status: true,
                    message: 'login success!',
                    data: payload,
                    tokenAccess
                });
            } else {
                const payload = {
                    id: findEmail.id,
                    email: findEmail.email,
                    role: findEmail.role
                };

                const tokenAccess = jwt.sign(payload, JWT_SECRET_KEY);

                return res.status(200).json({
                    status: true,
                    message: 'login success!',
                    data: payload,
                    tokenAccess
                });
            }


        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    },

    forgot: async (req, res) => {
        try {
            const { email } = req.body;

            const User = await user.findOne({ where: { email } });
            if (User) {
                const payload = {
                    id: User.id,
                    name: User.name,
                    email: User.email
                };

                const secret = JWT_SECRET_KEY + User.password;
                const token = await jwt.sign(payload, secret, { expiresIn: '15m' });

                const link = `${req.protocol}://${req.get('host')}/api/v1/auth/reset-password/${User.id}?token=${token}`;

                const html = `
                <p>hi ${User.name}, silahkan klik <a href="${link}" target="_blank">disini</a> untuk mengatur ulang kata sandi anda.</p>
                <p>link : ${link}</p>
                `;

                helper.sendEmail(User.email, 'Reset Password', html);
            }

            return res.json({
                status: true,
                message: 'email reset password akan dikirimkan jika email tersedia di dalam database.',
                data: null
            });

        } catch (err) {
            return res.status(500).json({
                status: true,
                message: err.message,
                data: null
            });
        }
    },

    reset: async (req, res) => {
        try {
            const token = req.query.token;
            const user_id = req.params.userId;
            const { new_password, confirm_new_password } = req.body;

            if (new_password != confirm_new_password) {
                return res.status(400).json({
                    status: false,
                    message: 'konfirmasi password harus sama dengan password baru',
                    data: null
                });
            }

            const User = await user.findOne({ where: { id: user_id } });

            const secret = JWT_SECRET_KEY + User.password;
            const data = await jwt.verify(token, secret);

            const encryptedPassword = await bcrypt.hash(new_password, 10);

            let query = {
                where: {
                    email: data.email
                }
            };

            let updated = await user.update({
                password: encryptedPassword
            }, query);

            return res.json({
                status: true,
                message: 'password berhasil diganti',
                data: updated
            });

        } catch (err) {
            return res.status(500).json({
                status: true,
                message: err.message,
                data: null
            });
        }
    },
}
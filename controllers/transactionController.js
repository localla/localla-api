const { user, product, transaction, product_images } = require('../models');
const bcrypt = require('bcrypt');
const Validator = require('fastest-validator');
const v = new Validator();
const { imagekit } = require('../helpers');

module.exports = {
    transaction: async (req, res) => {
        try {
            const user_id = req.user.id;

            let User = await user.findOne({
                where: {
                    id: user_id
                }
            })
            if (!User) {
                return res.status(404).json({
                    status: false,
                    message: 'user does not exist',
                    data: null
                })
            }

            const product_id = req.params.productid;

            let Product = await product.findOne({
                raw: true,
                where: {
                    id: product_id
                }
            })

            if (!Product) {
                return res.status(404).json({
                    status: false,
                    message: 'product does not exist',
                    data: null
                })
            }

            let isTransactionExists = await transaction.findOne({
                where: {
                    product_id: product_id,
                    user_id: user_id,
                    status: 'Pending'
                }
            })

            if (isTransactionExists) {
                return res.status(400).json({
                    status: false,
                    message: 'you has order for this product'
                })
            }

            const { price } = req.body;

            const newTransaction = await transaction.create({
                product_id: product_id,
                seller_id: Product.user_id,
                user_id: user_id,
                price,
                status: "Pending",
                transactionDate: Date.now(),
            })

            res.status(200).json({
                status: true,
                message: 'create transaction successfully',
                data: newTransaction
            })
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    allTransaction: async (req, res) => {
        try {
            const user_id = req.user.id;

            // const Trans = await product.findAll({
            //     where: {
            //         user_id: user_id
            //     },
            //     attributes: ['id', 'title', 'category', 'description', 'price'],
            //     include: [
            //         {
            //             model: product_images,
            //             as: "image_url",
            //             attributes: ['url']
            //         },
            //         {
            //             model: user,
            //             as: 'owner',
            //             attributes: ['nameShop', 'province', 'city', 'imageShop', 'phone'],
            //         },
            //         {
            //             model: transaction,
            //             as: 'transaction',
            //             attributes: ['id', 'product_id', 'user_id', 'price', 'status'],
            //             order: [
            //                     ['id', 'DESC'],
            //                 ],
            //         },
            //     ],
            //     // order: [
            //     //     ['id', 'DESC'],
            //     // ],
            // })

            const Trans = await transaction.findAll({
                where: {
                    seller_id: user_id,
                },
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                },
                include: [
                    {
                        model: user,
                        as: "buyer",
                        attributes: ['name', 'province', 'city', 'image', 'phone']
                    },
                    {
                        model: user,
                        as: "owner",
                        attributes: ['nameShop', 'province', 'city', 'imageShop', 'phone']
                    },
                    {
                        model: product,
                        as: "detailproduct",
                        attributes: ['id', 'title', 'category', 'description', 'price']
                    },
                    {
                        model: product_images,
                        as: "image_url",
                        attributes: ['url']
                    },
                ],
                order: [
                    ['id', 'DESC'],
                ],
            })

            return res.status(200).json({
                status: true,
                message: "All transaction",
                data: Trans,
            });
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    historyUserTransaction: async (req, res) => {
        try {
            const user_id = req.user.id;

            const Trans = await transaction.findAll({
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                },
                where: {
                    user_id: user_id
                },
                include: [
                    {
                        model: user,
                        as: "owner",
                        attributes: ['nameShop', 'province', 'city', 'imageShop', 'phone']
                    },
                    {
                        model: product,
                        as: "detailproduct",
                        attributes: ['id', 'title', 'category', 'description', 'price']
                    },
                    {
                        model: product_images,
                        as: "image_url",
                        attributes: ['url']
                    },
                ],
                order: [
                    ['id', 'DESC'],
                ],
            })

            res.status(200).json({
                status: true,
                message: "Transaction history",
                data: Trans,
            });
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    historyProductTransaction: async (req, res) => {
        try {
            const product_id = req.params.productid;

            const Trans = await transaction.findAll({
                where: {
                    product_id: product_id
                },
                include: [
                    {
                        model: user,
                        as: "buyer",
                        attributes: ['name', 'province', 'city', 'image', 'phone']
                    },
                    {
                        model: product,
                        as: "detailproduct",
                        attributes: ['title', 'category', 'description', 'price']
                    },
                    {
                        model: product_images,
                        as: "image_url",
                        attributes: ['url']
                    },
                ],
                order: [
                    ['id', 'DESC'],
                ],
            })

            res.status(200).json({
                status: true,
                message: "Transaction history",
                data: Trans,
            });
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    statusTransaction: async (req, res) => {
        try {
            const trans_id = req.params.transid;

            const Trans = await transaction.findOne({
                where: {
                    id: trans_id
                }
            })

            if (!Trans) {
                return res.status(400).json({
                    status: false,
                    message: 'transaction does not exist',
                    data: null
                })
            }

            const product_id = Trans.product_id;
            const user_id = await product.findOne({
                where: {
                    id: product_id
                }
            })

            const { status } = req.body;
            if (user_id.user_id === req.user.id) {
                if (status == "Accept") {
                    const update = await transaction.update(
                        {
                            status
                        },
                        {
                            where: { id: trans_id },
                        }
                    );

                    const declined = await transaction.update(
                        {
                            status: "Declined"
                        },
                        {
                            where: {
                                product_id: Trans.product_id,
                                status: "Pending"
                            }
                        }
                    )
                    return res.status(200).json({
                        status: true,
                        message: "Status transaction Update",
                        data: update,
                    });
                } else if (status == "Declined") {
                    const update = await transaction.update(
                        {
                            status
                        },
                        {
                            where: { id: trans_id },
                        }
                    );

                    return res.status(200).json({
                        status: true,
                        message: "Status transaction Update",
                        data: update,
                    });
                }
                else if (status == "Sold") {
                    const update = await transaction.update(
                        {
                            status
                        },
                        {
                            where: { id: trans_id },
                        }
                    );

                    return res.status(200).json({
                        status: true,
                        message: "Status transaction Update",
                        data: update,
                    });
                }
                else if (status == "Cancel") {
                    const update = await transaction.update(
                        {
                            status
                        },
                        {
                            where: { id: trans_id },
                        }
                    );

                    return res.status(200).json({
                        status: true,
                        message: "Status transaction Update",
                        data: update,
                    });
                }
            } else {
                return res.status(401).json({
                    status: false,
                    message: 'this is not your product.'
                })
            }

        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    deleteTransaction: async (req, res) => {
        try {

            const trans_id = req.params.transid;

            const Trans = await transaction.findOne({
                where: {
                    id: trans_id
                }
            })

            if (!Trans) {
                return res.status(400).json({
                    status: false,
                    message: 'transaction does not exist',
                    data: null
                })
            }

            if (Trans.user_id === req.user.id) {
                const deleted = await transaction.destroy({
                    where: { id: trans_id },
                });

                return res.status(200).json({
                    status: true,
                    message: "Transaction Deleted",
                    data: deleted,
                });
            } else {
                return res.status(401).json({
                    status: false,
                    message: 'this is not your transaction.'
                })
            }
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },
}
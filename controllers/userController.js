const { user, product, transaction, wishlist, product_images, wishlist_count } = require('../models');
const bcrypt = require('bcrypt');
const Validator = require('fastest-validator');
const v = new Validator();
const { imagekit } = require('../helpers');

module.exports = {
    registerUser: async (req, res) => {
        try {
            const schema = {
                name: 'string|required|min:1',
                email: 'email|required',
                password: 'string|required|min:8'
            }

            const validate = v.validate(req.body, schema);
            if (validate.length) {
                return res.status(400).json({
                    status: false,
                    message: "bad request!",
                    data: validate
                });
            }

            const { name, email, phone, password } = req.body;

            const isEmailExists = await user.findOne({ where: { email } })
            if (isEmailExists) {
                return res.status(400).json({
                    status: false,
                    message: 'Email is already exists',
                    data: null
                })
            }

            const encrptedPassword = await bcrypt.hash(password, 10)
            const newUser = await user.create({
                ...req.body,
                password: encrptedPassword,
                role: 'buyer',
                city: null,
                address: null,
                image: null,
                nameShop: null,
                imageShop: null,
                userType: 'basic'
            })

            res.status(201).json({
                status: true,
                message: 'register successfully'
            })

        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    updateUser: async (req, res) => {
        try {
            const User_id = req.user.id;;
            const User = await user.findOne({ where: { id: User_id } })
            if (!User) {
                return res.status(400).json({
                    status: false,
                    message: 'user does not exist'
                })
            }

            const { name, phone, province, city, address } = req.body;
            let query = { where: { id: User_id } }

            if (req.file) {
                const file = req.file.buffer.toString("Base64");
                const namaFile = Date.now() + '-' + req.file.originalname;

                if (req.file.size > 5 * 1024 * 1024) {
                    return res.status(400).json({
                        status: false,
                        message: 'Size too large, max 5MB'
                    })
                }

                if (req.file.mimetype !== 'image/png' && req.file.mimetype !== 'image/jpg' && req.file.mimetype !== 'image/jpeg') {
                    return res.status(400).json({
                        status: false,
                        message: "Supported image files are jpeg, jpg, and png"
                    })
                }

                const upload = await imagekit.upload({
                    file: file,
                    fileName: namaFile,
                    folder: '/localla/user/images'
                });

                let updated = await user.update({
                    name,
                    phone,
                    province,
                    city,
                    address,
                    image: upload.url,
                    role: 'seller'
                }, query)

                res.status(200).json({
                    status: true,
                    message: 'change profile updated',
                    data: updated
                })
            } else {
                let updated = await user.update({
                    name,
                    phone,
                    province,
                    city,
                    address,
                    role: 'seller'
                }, query)

                res.status(200).json({
                    status: true,
                    message: 'change profile updated',
                    data: updated
                })
            }
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    updatePassword: async (req, res) => {
        try {
            const User_id = req.user.id;

            const User = await user.findOne({ where: { id: User_id } })
            if (!User) {
                return res.status(400).json({
                    status: false,
                    message: 'user does not exist'
                })
            }
            const { password, new_password, confirm_new_password } = req.body;

            const passwordCompare = await bcrypt.compare(password, User.password);
            if (!passwordCompare) {
                return res.status(400).json({
                    status: false,
                    message: 'Wrong old password!'
                })
            }

            if (new_password != confirm_new_password) {
                return res.status(400).json({
                    status: false,
                    message: 'Confirm password must be the same as the new password'
                });
            }

            const encrptedPassword = await bcrypt.hash(new_password, 10)
            let query = { where: { id: User_id } }

            let updated = await user.update({
                password: encrptedPassword,
            }, query)

            return res.status(200).json({
                status: true,
                message: 'password changed successfully',
                data: updated
            })
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    allUser: async (req, res) => {
        try {
            const Users = await user.findAll({
                attributes: { exclude: ['password', 'createdAt', 'updatedAt'] }
            });
            res.status(200).json({
                status: true,
                message: "All Data User",
                data: Users,
            });
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    detailUser: async (req, res) => {
        try {

            const user_id = req.params.userid;

            let User = await user.findOne({
                attributes: { exclude: ['password', 'role'] },
                where: {
                    id: user_id
                }
            })
            if (!User) {
                return res.status(404).json({
                    status: false,
                    message: 'user does not exist',
                    data: null
                })
            }

            res.status(200).json({
                status: true,
                message: 'get detail user success',
                data: User
            })
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    detailByToken: async (req, res) => {
        try {
            const user_id = req.user.id;
            console.log(user_id)
            let User = await user.findOne({
                attributes: { exclude: ['password', 'role'] },
                include: [
                    {
                        model: wishlist,
                        as: "wishproduct",
                        attributes: ['product_id']
                    }
                ],
                where: {
                    id: user_id
                }
            })
            if (!User) {
                return res.status(404).json({
                    status: false,
                    message: 'user does not exist',
                    data: null
                })
            }

            res.status(200).json({
                status: true,
                message: 'get detail user success',
                data: User
            })
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    updateBackground: async (req, res) => {
        try {
            const User_id = req.user.id;;
            const User = await user.findOne({ where: { id: User_id } })
            if (!User) {
                return res.status(400).json({
                    status: false,
                    message: 'user does not exist'
                })
            }

            let query = { where: { id: User_id } }

            if (req.file) {
                const file = req.file.buffer.toString("Base64");
                const namaFile = Date.now() + '-' + req.file.originalname;

                if (req.file.size > 5 * 1024 * 1024) {
                    return res.status(400).json({
                        status: false,
                        message: 'Size too large, max 5MB'
                    })
                }

                if (req.file.mimetype !== 'image/png' && req.file.mimetype !== 'image/jpg' && req.file.mimetype !== 'image/jpeg') {
                    return res.status(400).json({
                        status: false,
                        message: "Supported image files are jpeg, jpg, and png"
                    })
                }

                const upload = await imagekit.upload({
                    file: file,
                    fileName: namaFile,
                    folder: '/localla/user/images'
                });

                let updated = await user.update({
                    imageBackground: upload.url,
                }, query)

                return res.status(200).json({
                    status: true,
                    message: 'change profile updated',
                    data: updated
                })
            } else {
                let updated = await user.update({
                    name,
                    phone,
                    city,
                    address,
                    role: 'seller'
                }, query)

                res.status(200).json({
                    status: true,
                    message: 'change profile updated',
                    data: updated
                })
            }
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            })
        }
    },

    getWishlist: async (req, res) => {
        try {
            const user_id = req.user.id;
            const User = await user.findOne({ where: { id: user_id } })
            if (!User) {
                return res.status(400).json({
                    status: false,
                    message: 'user does not exist'
                })
            }

            const wishlistfind = await wishlist.findAll({
                where: {
                    user_id: user_id,
                },
                include: [
                    {
                        model: user,
                        as: "owner",
                        attributes: ['nameShop', 'province', 'city', 'imageShop']
                    },
                    {
                        model: product,
                        as: "detailproduct",
                        attributes: ['id', 'title', 'category', 'description', 'price']
                    },
                    {
                        model: product_images,
                        as: "image_url",
                        attributes: ['url']
                    },
                    {
                        model: wishlist_count,
                        as: 'productWishlist',
                        attributes: ['total_wishlist']
                    },
                ]
            })

            return res.status(200).json({
                status: true,
                message: "Get all wishlist successfully",
                data: wishlistfind,
            });

        } catch (err) {
            res.status(500).json({
                status: false,
                message: err.message,
                data: null,
            });
        }
    },
}
"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      transaction.belongsTo(models.product, {
        through:models.user,
        foreignKey: "product_id",
        as: "detailproduct",
      }),
      
        transaction.belongsTo(models.user, {
          foreignKey: "user_id",
          as: "buyer",
        });

      transaction.belongsTo(models.product_images, {
        foreignKey: 'product_id',
        as: 'image_url'
      })

      // transaction.belongsToMany(models.user, {
      //   through:models.product,
      //   foreignKey: 'id',
      //   otherKey: 'user_id',
      //   as: "owner",
      // });

      transaction.belongsTo(models.user, {
        foreignKey: 'seller_id',
        as: "owner",
      });

      // transaction.belongsTo(models.product, {
      //   through:models.user,
      //   foreignKey: "product_id",
      //   as: "productdetail",
      // });

    }
  }
  transaction.init(
    {
      product_id: DataTypes.INTEGER,
      seller_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      price: DataTypes.INTEGER,
      status: DataTypes.STRING,
      transactionDate: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "transaction",
    }
  );
  return transaction;
};

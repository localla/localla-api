'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      // user.belongsTo(models.wishlist, {
      //   foreignKey: "wishlist",
      //   as: 'wishproduct'
      // })

      user.hasMany(models.wishlist, { foreignKey: 'user_id', as:'wishproduct'})
    }
  }
  user.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    phone: DataTypes.STRING,
    province: DataTypes.STRING,
    city: DataTypes.STRING,
    address: DataTypes.STRING,
    image: DataTypes.STRING,
    imageBackground: DataTypes.STRING,
    role: DataTypes.STRING,
    nameShop: DataTypes.STRING,
    imageShop: DataTypes.STRING,
    userType: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};
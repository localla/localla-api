"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      product.hasMany(models.transaction, {
        foreignKey: "product_id",
        as: "transaction",
      });

      product.belongsTo(models.product, {
        foreignKey: "id",
        as: "detailproduct",
      }),

      product.belongsTo(models.user, {
        foreignKey: "user_id",
        as: "owner",
      });

      product.hasOne(models.product_images, {
        foreignKey: 'id',
        as: 'image_url'
      })

      product.hasOne(models.wishlist_count, {
        foreignKey: 'product_id',
        as: 'productWishlist'
      })

      product.hasMany(models.wishlist, { foreignKey: 'product_id', as:'wishlists'})
    }
  }
  product.init(
    {
      user_id: DataTypes.INTEGER,
      title: DataTypes.STRING,
      category: DataTypes.STRING,
      description: DataTypes.TEXT,
      price: DataTypes.INTEGER,
      image: DataTypes.INTEGER,
      status: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "product",
    }
  );
  return product;
};

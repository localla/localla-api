'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class wishlist extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      wishlist.belongsTo(models.product, {
        foreignKey: "product_id",
        as: "detailproduct",
      })

      wishlist.belongsTo(models.user, {
        foreignKey: "seller_id",
        as: "owner",
      });

      wishlist.belongsTo(models.product_images, {
        through:models.product,
        foreignKey: 'product_id',
        as: 'image_url'
      })

      wishlist.belongsTo(models.wishlist_count, {
        through:models.product,
        foreignKey: 'product_id',
        as: 'productWishlist'
      })
    }
  }
  wishlist.init({
    user_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
    seller_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'wishlist',
  });
  return wishlist;
};
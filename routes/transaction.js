const express = require('express');
const router = express.Router();

const { login } = require('../middlewares/index')
const upload = require('../middlewares/upload');

const transaction = require('../controllers/transactionController')

router.get('/',login, transaction.allTransaction)
router.get('/user',login, transaction.historyUserTransaction)
router.get('/product/:productid',login, transaction.historyProductTransaction)
router.post('/:productid',login, transaction.transaction)
router.put('/update/:transid',login, transaction.statusTransaction)
router.delete('/delete/:transid', login, transaction.deleteTransaction)

module.exports = router;
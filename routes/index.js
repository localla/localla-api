const express = require("express");
const user = require("./users");
const auth = require("./auth");
const product = require("./products");
const transaction = require("./transaction");
const seller = require('./seller')
const swaggerUi = require("swagger-ui-express");
const router = express.Router();
const swaggerDocument = require("../swagger.json");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.json('welcome')
});

router.use("/user", user);
router.use("/auth", auth);
router.use("/product", product);
router.use("/transaction", transaction);
router.use("/seller", seller);

/* GET documentation page. */
router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

module.exports = router;

const express = require("express");
const router = express.Router();
const product = require("../controllers/productController");

const upload = require('../middlewares/upload');
const multi = require('../middlewares/multiUpload');
const { login } = require("../middlewares");

router.post("/", login, multi, product.createProduct);
router.post("/wishlist/:productid", login, product.wishlist);
// router.get('/wishlist', login, user.getWishlist)
router.get("/", product.allProduct);
router.get("/seller/:userid", product.allProductByUserId);
router.get("/:productid", product.detailProduct);
router.put("/:productid", login, multi, product.updateProduct);
router.delete("/:productid", login, product.deleteProduct);


module.exports = router;

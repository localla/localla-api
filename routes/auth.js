const express = require('express');
const router = express.Router();

const auth = require('../controllers/authController')

router.post('/login', auth.login)
router.get('/google', auth.googleOAuth)
router.post('/forgot-password', auth.forgot);
router.post('/reset-password/:userId', auth.reset);

module.exports = router;
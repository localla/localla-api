const express = require('express');
const router = express.Router();

const { login } = require('../middlewares/index')
const upload = require('../middlewares/upload');

const seller = require('../controllers/sellerController')

router.put('/', login, upload, seller.registerSeller)

module.exports = router;

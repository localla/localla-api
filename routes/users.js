const express = require('express');
const router = express.Router();

const { login } = require('../middlewares/index')
const upload = require('../middlewares/upload');

const user = require('../controllers/userController')

router.get('/', login, user.allUser)
router.get('/wishlist', login, user.getWishlist)
router.get('/profile', login, user.detailByToken)
router.get('/:userid',  user.detailUser)
router.post('/register', user.registerUser)
router.put('/', login, upload, user.updateUser)
router.put('/image', login, upload, user.updateBackground)
router.put('/updatepassword', login, user.updatePassword)

module.exports = router;

require("dotenv").config();
const { DB_USERNAME, DB_PASSWORD, DB_NAME, DB_HOST, DB_DIALECT } = process.env;

module.exports = {
  development: {
    username: DB_USERNAME || "postgres",
    password: DB_PASSWORD || "password",
    database: DB_NAME || "dbname",
    host: DB_HOST || "100.0.0.0",
    dialect: DB_DIALECT || "postgres",
    protocol: "postgres",
  },

  staging: {
    username: DB_USERNAME || "postgres",
    password: DB_PASSWORD || "password",
    database: DB_NAME || "dbname",
    host: DB_HOST || "100.0.0.0",
    dialect: DB_DIALECT || "postgres",
    protocol: "postgres",
  },

  production: {
    use_env_variable: "DATABASE_URL",
    dialect: "postgres",
    protocol: "postgres",
    ssl: true,
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
};
